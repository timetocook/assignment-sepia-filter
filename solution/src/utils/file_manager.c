#include <stdio.h>
#include <stdlib.h>

#include "utils/print_manager.h"
#include "utils/file_manager.h"

#define READ_ERROR 1


struct file_status *file_status_init(int code, char *message, FILE *file) {
	struct file_status *result_file_status = malloc(sizeof(struct file_status));
	result_file_status->code = code;
	result_file_status->message = message;
	result_file_status->file = file;
	return result_file_status;
}

void free_file_status(struct file_status *file){
	fprintf(stderr, "free file\n");
	fclose(file->file);
	fprintf(stderr, "free messag\n");
	//free(file->message);
	fprintf(stderr, "end frre file status\n");
	free(file);
}

struct file_status *open_file(char const* path, char const* mode) {
	FILE *file = fopen(path, mode);
	int code = OPEN_OK;
	char *message = "FIle opened successfully.";
	if (file == NULL) {
		code = OPEN_FAILED;
		message = "File opening occured.";
	}
	return file_status_init(code, message, file);
}

struct file_status *read_from_file(void *buffer, FILE *input_file) {
	size_t length = fread(buffer, sizeof(buffer), 1, input_file);
	int code = READ_OK;
	char *message = "FIle read successfully.";
	if (length < 1) {
		code = READ_INVALID_BITS;
		message = "Error during reading.";
	}
	return file_status_init(code, message, NULL);
}

struct file_status *write_to_file(void *buffer, FILE *output_file) {
	size_t length = fwrite(buffer, sizeof(buffer), 1, output_file);
	int code = WRITE_OK;
	char *message = "FIle written successfully.";
	if (length < 1) {
		code = READ_ERROR;
		message = "Error during writing.";
	}
	return file_status_init(code, message, NULL);
}

struct file_status *seek_of_file(uint32_t offset_bits, FILE *output_file) {
	int length = fseek(output_file, offset_bits, SEEK_SET);
	int code = WRITE_OK;
	char *message = "FIle written successfully.";
	if (length != 0) {
		code = READ_ERROR;
		message = "Error during writing.";
	}
	return file_status_init(code, message, NULL);
}

