#include "formats/bmp.h"
#include "formats/image.h"
#include "transform/sepia.h"
#include "utils/print_manager.h"
#include "utils/file_manager.h"
#include <stdio.h>

#define ARGUMENT_OK_CODE 0
#define NO_ARGUMENTS_COMMAND_LINE 1
#define ONLY_ONE_ARGUMENT_COMMAND_LINE 2
#define REQUIRED_COUNT_ARGUMENTS 3

/* Обрабатывает количество аргументов командной строки */
void check_argument_count(int argc) {
	switch(argc) {
		case NO_ARGUMENTS_COMMAND_LINE:
			print_exception(NO_ARGUMENTS_COMMAND_LINE, 
			"Please, enter an input-file and output-file.");
			break;
		case ONLY_ONE_ARGUMENT_COMMAND_LINE:
			print_exception(ONLY_ONE_ARGUMENT_COMMAND_LINE, 
			"Please, enter an output-file.");
			break;
		case REQUIRED_COUNT_ARGUMENTS:
			print_exception(ARGUMENT_OK_CODE, 
			"Parsed 2 argument, it's ok.");
			break;
		default:
			print_exception(NO_ARGUMENTS_COMMAND_LINE, 
			"There are invalid count parameters, exclude it.");
			break;
	}
} 

int main( int argc, char **argv ) {
	/* Проверяем количество аргументов командной строки */
	fprintf(stderr, "start main");
    	check_argument_count(argc);
	
	/* Открываем файл на чтение, выкидываем ошибку, если что-то не так */
    fprintf(stderr, "start opening");
	struct file_status *open_result = 
		open_file(argv[1], "rb");
	print_exception(open_result->code, open_result->message);
	
	/* Читаем картинку из файла, выкидываем ошибку, если что-то не так */
    fprintf(stderr, "start image");
	struct image_status *image_input_result = 
		from_bmp(open_result->file);
	print_exception(image_input_result->code, image_input_result->message);

    fprintf(stderr, "free file status1");
	free_file_status(open_result);
	
	/* Поворачиваем, выкидываем ошибку, если что-то не так */
    fprintf(stderr, "imagestatus 2");
	struct image_status *image_result = 
		sepia(image_input_result->content);
	print_exception(image_result->code, image_result->message);

    fprintf(stderr, "@@%" PRIu64 "@@", image_result->content->width);
	free_image_status(image_input_result);
	
	/* Открываем файл на запись, выкидываем ошибку, если что-то не так */
    fprintf(stderr, "start reading file");
	open_result = 
		open_file(argv[2], "wb");
	print_exception(open_result->code, open_result->message);
	
	/* Пытаемся записать картинку в файл, выкидываем ошибку, если что-то не так */
    fprintf(stderr, "start tobmp");
	struct image_status *image_output_result = 
		to_bmp(open_result->file, image_result->content);
	print_exception(image_result->code, image_result->message);

    fprintf(stderr, "free file status s1");
	free_file_status(open_result);

    fprintf(stderr, "end");
	free_image_status(image_result);
	free_image_status(image_output_result);
    fprintf(stderr, "TOTALLYEND\n");
	
	return 0;
}
