#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "utils/print_manager.h"
#include "formats/image.h"
#include "formats/bmp.h"
#include "utils/file_manager.h"

#define BIT_COUNT_DEFAULT 24

static uint8_t *make_offset_buffer(uint32_t width) {
	uint8_t offset = 4 - (sizeof(struct pixel) * width) % 4;
	return malloc(sizeof(uint8_t)*offset);
}

struct image *fill_image(bmp_header const* header, FILE *input_file) {
	struct image *img = image_init(header->biWidth, header->biHeight);
	
	uint8_t *offset_buffer = make_offset_buffer(header-> biWidth);
	struct pixel* pxl = malloc(sizeof(struct pixel));
	struct file_status *pixel_read_status;
	struct file_status *offset_read_status;
	
	for (size_t i = 0; i < header->biHeight; i++) {
		for (size_t j = 0; j < header->biWidth; j++) {
			fread(pxl, sizeof(struct pixel), 1, input_file);
			pixel_read_status =
			read_from_file(pxl, input_file);
			print_exception
			(pixel_read_status->code, pixel_read_status->message);
			
			img->data[header->biWidth * i + j] = *pxl;
		}
		if (sizeof(offset_buffer) != 4) {
			fread(offset_buffer, sizeof(offset_buffer), 1, input_file);
			offset_read_status =
			read_from_file(offset_buffer, input_file);
			print_exception
			(offset_read_status->code, offset_read_status->message);
		}
	}
	
	free_file_status(pixel_read_status);
	free_file_status(offset_read_status);
	free(offset_buffer);
	free(pxl);
	
	return img;
}

void unfill_image(struct image const* img, FILE *output_file) {
	struct pixel *pxl = malloc(sizeof(struct pixel));
	struct file_status *pixel_write_status;
	struct file_status *clear_write_status;
	uint8_t *offset_buffer = make_offset_buffer(img->width);
	
	for (size_t i = 0; i < img->height; i++) {
		for (size_t j = 0; j < img->width; j++) {
			*pxl = img->data[img->width * i + j];
			fwrite(pxl, sizeof(struct pixel), 1, output_file);
			pixel_write_status =
			write_to_file(pxl, output_file);
			print_exception
			(pixel_write_status->code, pixel_write_status->message);
		}
		if (sizeof(offset_buffer) != 4) {
			clear_write_status = write_to_file(offset_buffer, output_file);
			print_exception
			(clear_write_status->code, clear_write_status->message);
		}
	}
	free(pxl);
	free(offset_buffer);
	free_file_status(pixel_write_status);
	free_file_status(clear_write_status);
}

bmp_header *fill_header(uint64_t width, uint64_t height) {
	bmp_header *header = malloc(sizeof(bmp_header));
	
	header->bfType = 0x4D42;
	header->bfReserved = 0;
	header->bOffBits = 54;
	header->biSize = 40;
	header->biWidth = width;
	header->biHeight = height;
	header->biPlanes = 1;
	header->biBitCount = 24;
	header->biCompression = 0;
	uint8_t *offset_buffer = make_offset_buffer(header->biWidth);
	header->biSizeImage = 
	(width * sizeof(struct pixel) + sizeof(offset_buffer) % 4) * height;
	header->bfileSize = header->biSizeImage + sizeof(bmp_header);
	header->biXPelsPerMeter = 0;
	header->biYPelsPerMeter = 0;
	header->biClrUsed = 0;
	header->biClrImportant = 0;

	return header;
}

struct image_status *from_bmp(FILE *input_file) {
    bmp_header header;
    struct pixel pixel;
    uint8_t offset;
    fread(&header, sizeof(bmp_header), 1, input_file);

    if(header.biBitCount != 24){
        return image_status_init(READ_INVALID_BITS, "FAILED", NULL);
    }

    struct image *img = image_init(header.biWidth, header.biHeight);
    offset = (3*header.biWidth) % 4;
    if (offset != 0) {
        offset = 4 - offset;
    }

    for (size_t i = 0; i < header.biHeight; i++){
        for (size_t j = 0; j < header.biWidth; j++) {
            fread(&pixel, sizeof(struct pixel), 1, input_file);
            // printf("%d(%zu; %zu) - r:%d, g:%d, b:%d\n", (uint32_t)((*img)->width * i + j), j, i,pixel.r, pixel.g, pixel.b);
            img->data[header.biWidth * i + j] = pixel;
        }

        if (offset != 0){
            fread(&pixel, sizeof(unsigned char)*offset, 1, input_file);
        }
    }
	
	return image_status_init(READ_OK, "Image is being correctly read.", img);
}

struct image_status *to_bmp(FILE *output_file, struct image const* img) {
    size_t padding = (4 - (img->width * sizeof(struct pixel) % 4)) % 4;
    bmp_header header = {0};
    struct pixel pixel;

    header.bfType = 0x4D42;
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    header.bfileSize = header.biSizeImage + sizeof(header);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    uint8_t offset = (4 - (header.biWidth * sizeof(struct pixel) % 4)) % 4;

    fwrite(&header, sizeof(bmp_header), 1, output_file);
    fseek(output_file, header.bOffBits, SEEK_SET);

    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            pixel = img->data[img->width * i + j];
            // printf("%lu - r:%d, g:%d, b:%d\n", img->width * i + j, pixel.r, pixel.g, pixel.b);
            fwrite(&pixel, sizeof(struct pixel), 1, output_file);
        }
        if (offset != 0) {
            for (size_t j = 0; j < offset; j++) {
                pixel.b = 0;
                pixel.r = 0;
                pixel.g = 0;
                fwrite(&pixel, sizeof(unsigned char), 1, output_file);
            }
        }
    }
    return image_status_init(WRITE_OK, "OK", NULL);
	
}
