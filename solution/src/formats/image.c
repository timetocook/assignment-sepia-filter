#include "formats/image.h"
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

/* Инициализация картинки */
struct image *image_init(uint64_t width, uint64_t height) {
	struct image *img = malloc(sizeof(struct image));
	img->width = width;
	img->height = height;
	img->data = malloc(sizeof(struct pixel) * width * height);
	return img;
}

/* Освобождение картинки */
void free_image(struct image* img){
    if (img!=NULL) {
        free(img->data);
        free(img);
    }
}

/* Оборачивание картинки в полноценную структуру */
struct image_status *image_status_init
(int code, char* message, struct image* image) {
	struct image_status *status = malloc(sizeof(struct image_status));
	status->code = code;
	status->message = message;
	status->content = image;
	return status;
}

/* Освобождение image_status */
void free_image_status(struct image_status *status){
	fprintf(stderr, "start free image status\n");
	free_image(status->content);
	//free(status->message);
	fprintf(stderr, "free status\n");
	free(status);
}

