#include "formats/image.h"

#ifndef SEPIA_H
#define SEPIA_H
struct image_status *sepia(struct image const* input_image);
#endif
