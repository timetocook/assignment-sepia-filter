#include <inttypes.h>
#ifndef STRUCT_IMAGE_H
#define STRUCT_IMAGE_H
struct pixel {
	uint8_t r, g, b;
};
struct image {
	uint64_t width, height;
	struct pixel *data; 
};
struct image_status {
	int code;
	char *message;
	struct image *content;	
};
#endif
struct image *image_init(uint64_t width, uint64_t height);
void free_image(struct image* img);
struct image_status *image_status_init
(int code, char* message, struct image * image);
void free_image_status(struct image_status *status);

